# vim:set syntax=dockerfile:
FROM alpine:latest

# application dependencies, we add them here to support better layer caching.
# hadolint ignore=DL3008
RUN apk add --no-cache \
  ca-certificates \
  curl \
  unzip \
  fuse

ARG BUILD_DATE
ARG VCS_URL
ARG VCS_REF
ARG ARCH=amd64

# container metadata
LABEL org.label-schema.schema-version="1.0" \
  org.label-schema.name="mcf.io Rclone - Rclone ('rsync for cloud storage') is a command line program to sync files and directories to and from different cloud storage providers." \
  org.label-schema.build-date="${BUILD_DATE}" \
  org.label-schema.url="https://rclone.org/" \
  org.label-schema.vcs-url="${VCS_URL}" \
  org.label-schema.vcs-ref="${VCS_REF}"

ENV XDG_CONFIG_HOME=/config

RUN mkdir -p "/tmp/rclone.install" && cd "/tmp/rclone.install" && \
  RCLONE_VERSION="$(curl -sX GET https://downloads.rclone.org/version.txt | awk '{print $2}')" && \
  curl -fsSL -O "https://downloads.rclone.org/${RCLONE_VERSION}/rclone-${RCLONE_VERSION}-linux-${ARCH}.zip" && \
  unzip "rclone-${RCLONE_VERSION}-linux-${ARCH}.zip" && \
  cp "rclone-${RCLONE_VERSION}-linux-${ARCH}/rclone" /usr/bin/rclone && \
  chown root:root /usr/bin/rclone && \
  chmod u=rwX,go=rX /usr/bin/rclone && \
  cd / && rm -rf /tmp/rclone.install


ENTRYPOINT [ "rclone" ]
WORKDIR /data
